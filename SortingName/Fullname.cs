﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingName
{
    public class Fullname : IComparable
    {
        public string FirstName { get; }
        public string LastName { get; }
        public Fullname(string FName, string LName)
        {
            this.FirstName = FName;
            this.LastName = LName;
        }

        public int CompareTo(object obj)
        {
            Fullname fullname = (Fullname)obj;
            if (String.Compare(this.LastName, fullname.LastName) == 0)
            {
                return String.Compare(this.FirstName, fullname.FirstName);
            }
            return String.Compare(this.LastName, fullname.LastName);

        }

        public override string ToString()
        {
            return FirstName + LastName;
        }
    }
}
