﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingName
{
    public class SortingData : ISortingData
    {
        List<Fullname> _fullnames;
        public SortingData(List<Fullname> fullnames)
        {
            _fullnames = fullnames;
        }
        public List<Fullname> DoSort()
        {
            _fullnames.Sort();
            return _fullnames;
        }
    }
}
