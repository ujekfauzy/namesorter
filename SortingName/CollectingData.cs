﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingName
{
    public class CollectingData : ICollectData
    {
        string _pathFile;
        public CollectingData(string path)
        {
            this._pathFile = path;
        }
        public List<Fullname> DoCollect()
        {
            List<Fullname> fullnames = new List<Fullname>();
            try
            {
                if (this._pathFile == null)
                {
                    throw new Exception("No File Input");
                }
                //Processing txt file into string and mapping on object fullname
                string[] lines = System.IO.File.ReadAllLines(_pathFile);
                {
                    foreach (var item in lines)
                    {
                        ScreeningData screeningData = new ScreeningData(item);
                        if (screeningData.DoScreening() != null)
                        {
                            fullnames.Add(screeningData.DoScreening());
                        }

                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            return fullnames;
        }

    }
}
