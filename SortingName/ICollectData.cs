﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingName
{
    interface ICollectData
    {
        List<Fullname> DoCollect();
    }
}
