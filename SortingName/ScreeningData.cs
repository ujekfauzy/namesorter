﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingName
{
    public class ScreeningData : IScreeningData
    {
        string _text;
        public ScreeningData(string text)
        {
            this._text = text;
        }

        public Fullname DoScreening()
        {
            if (!string.IsNullOrEmpty(_text) && !string.IsNullOrWhiteSpace(_text))
            {
                var sb = new StringBuilder();

                char previousChar = char.MinValue; // Unicode '\0'

                foreach (char c in _text)
                {
                    if (char.IsUpper(c))
                    {
                        // If not the first character and previous character is not a space, insert a space before uppercase

                        if (sb.Length != 0 && previousChar != ' ')
                        {
                            sb.Append(' ');
                        }
                    }

                    sb.Append(c);

                    previousChar = c;
                }

                //Split  string to become first name and last name
                var res = sb.ToString().Split(' ');
                string fname = string.Empty;
                string lname = string.Empty;
                lname = res.LastOrDefault();
                if (res.Length >= 2)
                {
                    fname = string.Join(" ", res.Take(res.Length - 1).ToArray());

                }
                return new Fullname(fname, lname);
            }
            else
            {
                return null;
            }
        }
    }
}
