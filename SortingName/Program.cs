﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingName
{
    class Program
    {
        static void Main(string[] args)
        {
            //string fileInputName = "unsorted-names-list.txt";
            string path = Path.Combine(Environment.CurrentDirectory, args[0]); //you can change args[0] while you run manually in visual studio
            
            //Colecting data from path file
            CollectingData collectingData = new CollectingData(path);

            //Sorting data
            SortingData sortingData = new SortingData(collectingData.DoCollect());
            string fileOutputName = "sorted-names-list.txt";
            string pathSave = Path.Combine(Environment.CurrentDirectory, fileOutputName);
            
            //Save and print in screen
            FileStream stream = null;
            try
            {
                // Create a FileStream with mode CreateNew  
                stream = new FileStream(pathSave, FileMode.OpenOrCreate);
                // Create a StreamWriter from FileStream  
                using (StreamWriter writer = new StreamWriter(stream, Encoding.UTF8))
                {
                    foreach (var item in sortingData.DoSort())
                    {
                        writer.WriteLine(item.ToString());
                    }
                }
            }
            finally
            {
                if (stream != null)
                    stream.Dispose();
            }
           
            // Read a file  
            string readText = File.ReadAllText(fileOutputName);
            Console.WriteLine(readText);
            Console.ReadKey();
        }
    }
}
