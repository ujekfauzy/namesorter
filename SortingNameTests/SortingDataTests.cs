﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SortingName;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingName.Tests
{
    [TestClass()]
    public class SortingDataTests
    {
        [TestMethod()]
        public void SortingDataTest()
        {
            //Arrange
            List<Fullname> fullnames = new List<Fullname>();
            fullnames.Add(new Fullname("Ahmad Natsir", "Fauzi"));
            fullnames.Add(new Fullname("Michael", "Heart"));
            fullnames.Add(new Fullname("No Name", "Test"));

            //Act 
            var obj = new SortingData(fullnames);
            //Assert
            Assert.IsInstanceOfType(obj, typeof(SortingData));
        }

        [TestMethod()]
        public void DoSortTest()
        {
            //Arrange
             List<Fullname> fullnames = new List<Fullname>();

            fullnames.Add(new Fullname("A", "Kohler"));
            fullnames.Add(new Fullname("Ahmad Natsir", "Fauzi"));
            fullnames.Add(new Fullname("Michael", "Heart"));
            fullnames.Add(new Fullname("No Name", "Test"));

            List<Fullname> expectation = new List<Fullname>();
            expectation.Add(new Fullname("Ahmad Natsir", "Fauzi"));
            expectation.Add(new Fullname("Michael", "Heart"));
            expectation.Add(new Fullname("A", "Kohler"));
            expectation.Add(new Fullname("No Name", "Test"));
            var obj = new SortingData(fullnames);

            //Act 
            var res = obj.DoSort();
            int i = 0;
            var _res = true;
            foreach (var item in res)
            {
                if (string.Compare(item.FirstName, expectation[i].FirstName) != 0 || string.Compare(item.LastName, expectation[i].LastName) != 0)
                {
                    _res = false;
                    break;
                }
                i++;
            }
            //Assert
           
           
            Assert.IsTrue(_res);
        }
    }
}