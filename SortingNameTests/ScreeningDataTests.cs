﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SortingName;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingName.Tests
{
    [TestClass()]
    public class ScreeningDataTests
    {
        [TestMethod()]
        public void ScreeningDataTest()
        {
            //Arrange
            string text = "test name 1";
            // Act
            var obj = new ScreeningData(text);
            //Assert
            Assert.IsInstanceOfType(obj, typeof(ScreeningData) );
        }

        [TestMethod()]
        public void DoScreeningTest()
        {
            //Arrange
            string text = "Test Name1";
            //Act
            var obj = new ScreeningData(text).DoScreening();
            //Assert
            Assert.IsInstanceOfType(obj, typeof(Fullname));
            
        }
    }
}