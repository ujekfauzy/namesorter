﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SortingName;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingName.Tests
{
    [TestClass()]
    public class CollectingDataTests
    {
        [TestMethod()]
        public void CollectingDataTest()
        {
            //Arrange
            string text = "path";
            // Act
            var obj = new CollectingData(text);
            //Assert
            Assert.IsInstanceOfType(obj, typeof(CollectingData));
        }

        [TestMethod()]
        public void DoCollectTest()
        {
            //Arrange
            string fileInputName = "unsorted-names-list-test.txt";
            string path = Path.Combine(Environment.CurrentDirectory, fileInputName);
            //string text = @"E:\Ujek\KSP\SortingName\SortingName\unsorted-names-list-test.txt";
            var obj = new CollectingData(path);
            List<Fullname> expectation = new List<Fullname>();

            expectation.Add(new Fullname("A", "Kohler"));
            expectation.Add(new Fullname("Ahmad Natsir", "Fauzi"));
            expectation.Add(new Fullname("Michael", "Heart"));
            expectation.Add(new Fullname("No Name", "Test"));

            // Act
            var res = obj.DoCollect();
            int i = 0;
            var _res = true;
            foreach (var item in res)
            {
                if (string.Compare(item.FirstName, expectation[i].FirstName) != 0 || string.Compare(item.LastName, expectation[i].LastName) != 0)
                {
                    _res = false;
                    break;
                }
                i++;
            }
            //Assert
            Assert.IsTrue(_res);
        }
    }
}